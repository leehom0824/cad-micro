module gitlab.com/mayunmeiyouming/cad-micro

go 1.14

require (
	github.com/golang/protobuf v1.4.2
	github.com/jinzhu/gorm v1.9.15
	github.com/prometheus/common v0.11.1
	github.com/twitchtv/twirp v5.12.1+incompatible
	google.golang.org/protobuf v1.25.0
)
