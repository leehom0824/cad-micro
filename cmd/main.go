package main

import (
	"fmt"
	"net/http"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gitlab.com/mayunmeiyouming/cad-micro/internal/repository"
	"gitlab.com/mayunmeiyouming/cad-micro/internal/server"
	cad "gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

func main() {
	database, err := gorm.Open("mysql", "newcomer:newcomer@(47.112.200.141:3306)/training_camp?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		fmt.Println(err)
		return
	} else {
		fmt.Println("connection succedssed")
	}
	defer database.Close()

	database.LogMode(true)

	server := &server.Server{
		Repository: &repository.Database{
			Client: *database,
		},
	}
	twirpHandler := cad.NewCadTwirpServer(server, nil)

	fmt.Println("ready perfectly, port:", 1234)
	http.ListenAndServe(":1234", twirpHandler)
}
