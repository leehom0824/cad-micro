package models

//Acquistions ...
type Acquistions struct {
	CompanyPermalink     string `gorm:"type:varchar(255);"`
	CompanyName          string `gorm:"type:varchar(255);"`
	CompanyCategoryCode  string `gorm:"type:varchar(255);"`
	CompanyCountryCode   string `gorm:"type:varchar(255);"`
	CompanyStateCode     string `gorm:"type:varchar(255);"`
	CompanyRegion        string `gorm:"type:varchar(255);"`
	CompanyCity          string `gorm:"type:varchar(255);"`
	AcquirerPermalink    string `gorm:"type:varchar(255);"`
	AcquirerName         string `gorm:"type:varchar(255);"`
	AcquirerCatagoryCode string `gorm:"type:varchar(255);"`
	AcquirerCountryCode  string `gorm:"type:varchar(255);"`
	AcquirerStateCode    string `gorm:"type:varchar(255);"`
	AcquirerRegion       string `gorm:"type:varchar(255);"`
	AcquirerCity         string `gorm:"type:varchar(255);"`
	AcquirerdAt          string `gorm:"type:varchar(255);"`
	AcquirerdMonth       string `gorm:"type:varchar(255);"`
	AcquirerdQuarter     string `gorm:"type:varchar(10);"`
	AcquirerdYear        int64
	PriceAmount          int64
	PriceCurrentCode     string `gorm:"type:varchar(255);"`
}
