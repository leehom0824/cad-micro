package models

//Investments ...
type Investments struct {
	CompanyPermalink     string `gorm:"type:varchar(255);"`
	CompanyName          string `gorm:"type:varchar(255);"`
	CompanyCategoryCode  string `gorm:"type:varchar(255);"`
	CompanyCountryCode   string `gorm:"type:varchar(255);"`
	CompanyStateCode     string `gorm:"type:varchar(255);"`
	CompanyRegion        string `gorm:"type:varchar(255);"`
	CompanyCity          string `gorm:"type:varchar(255);"`
	InvestorPermalink    string `gorm:"type:varchar(255);"`
	InvestorName         string `gorm:"type:varchar(255);"`
	InvestorCategoryCode string `gorm:"type:varchar(255);"`
	InvestorCountryCode  string `gorm:"type:varchar(255);"`
	InvestorStateCode    string `gorm:"type:varchar(255);"`
	InvestorRegion       string `gorm:"type:varchar(255);"`
	InvestorCity         string `gorm:"type:varchar(255);"`
	FundingRoundType     string `gorm:"type:varchar(255);"`
	FundedAt             string `gorm:"type:varchar(255);"`
	FundedMonth          string `gorm:"type:varchar(255);"`
	FundedQuarter        string `gorm:"type:varchar(255);"`
	FundedYear           int64
	RaisedAmountUsd      int64
}
