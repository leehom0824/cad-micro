package models

//Companies ...
type Companies struct {
	Permalink       string `gorm:"primary_key"`
	Name            string `gorm:"type:varchar(255);"`
	CategoryCode    string `gorm:"type:varchar(255);"`
	FundingTotal    int64
	CountryCode     string `gorm:"type:varchar(255);"`
	StateCode       string `gorm:"type:varchar(255);"`
	Region          string `gorm:"type:varchar(100);"`
	City            string `gorm:"type:varchar(100);"`
	FundingRounds   int64
	FoundedAt       string `gorm:"type:varchar(10);"`
	FoundedMonth    string `gorm:"type:varchar(10);"`
	FoundedQuarter  string `gorm:"type:varchar(10);"`
	FoundedYear     int64
	FirstFundedAt   string `gorm:"type:varchar(10);"`
	LastFundedAt    string `gorm:"type:varchar(10);"`
	LastMilestoneAt string `gorm:"type:varchar(10);"`
}
