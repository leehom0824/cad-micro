package server

import (
	context "context"

	"log"

	"gitlab.com/mayunmeiyouming/cad-micro/internal/models"
	"gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

// QueryCompanyOverviewByCompanyID 通过id查询公司概要信息
func (s *Server) QueryCompanyOverviewByCompanyID(ctx context.Context, request *cad.QueryCompanyOverviewByCompanyIDRequest) (*cad.CompanyOverviewResponse, error) {
	// Server的Repository就是Database结构体，可以通过这个Repository数据库层
	company, err := s.Repository.QueryCompanyInfoByCompanyID(request.CompanyId)
	if err != nil {
		return nil, err
	}

	// 查询首次融资信息
	var firstFunded *cad.FinancingInfo
	var firstRounds []models.Rounds
	if company.FirstFundedAt != "" {
		firstRounds, err = s.Repository.QueryFundedInfosByCompanyIDAndFundedAt(request.CompanyId, company.FirstFundedAt)
		if err != nil {
			return nil, err
		}
		log.Println("成功查询首轮融资信息")
		log.Println(firstRounds)
		log.Println("融资数：", len(firstRounds))
		if len(firstRounds) >= 1 {
			firstFunded = &cad.FinancingInfo{
				FundingRoundType: firstRounds[0].FundingRoundType,
				FundedAt:         firstRounds[0].FundedAt,
				RaisedAmountUsd:  int64(firstRounds[0].RaisedAmountUsd),
				CompanyId:        company.Permalink,
			}
		}
	}

	// 查询最后一次融资信息
	var lastFunded *cad.FinancingInfo
	var lastRounds []models.Rounds
	if company.LastFundedAt != "" {
		lastRounds, err = s.Repository.QueryFundedInfosByCompanyIDAndFundedAt(request.CompanyId, company.LastFundedAt)
		if err != nil {
			return nil, err
		}
		log.Println("成功查询最近一次融资信息")
		log.Println(lastRounds)
		log.Println("融资数：", len(lastRounds))
		if len(lastRounds) >= 1 {
			lastFunded = &cad.FinancingInfo{
				FundingRoundType: lastRounds[0].FundingRoundType,
				FundedAt:         lastRounds[0].FundedAt,
				RaisedAmountUsd:  int64(lastRounds[0].RaisedAmountUsd),
				CompanyId:        company.Permalink,
			}
		}
	}

	res := &cad.CompanyOverviewResponse{
		City:            company.City,
		FoundedAt:       company.FoundedAt,
		StatusCode:      company.StateCode,
		FirstFunded:     firstFunded,
		LastFunded:      lastFunded,
		FoundingRounds:  company.FundingRounds,
		FundingTotalUsd: company.FundingTotal,
	}

	return res, nil
}
