package server

import (
	context "context"
	"gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

// QueryCompanyListByCompanyName 该接口用于搜索页的模糊查找，通过输入的公司名查找拥有相同的字的公司信息
func (s *Server) QueryCompanyListByCompanyName(ctx context.Context, request *cad.QueryCompanyListByCompanyNameRequest) (*cad.CompanyInfoResponse, error) {
	companyList, err := s.Repository.QueryCompanyListByCompanyName(request.CompanyName)
	if err != nil {
		return nil, err
	}

	res := &cad.CompanyInfoResponse {
		CompanyInfo: make([]*cad.CompanyInfo, 0),
	}
	for _, value := range companyList {
		companyInfo := &cad.CompanyInfo {
			CompanyId: value.Permalink,
			CompanyName: value.Name,
			CategoryCode: value.CategoryCode,
		}
		res.CompanyInfo = append(res.CompanyInfo, companyInfo)
	}
	return res, nil
}