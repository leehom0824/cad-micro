package server

import (
	context "context"
	"errors"
	"strconv"

	"github.com/golang/protobuf/ptypes/wrappers"

	"gitlab.com/mayunmeiyouming/cad-micro/internal/models"
	"gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

//QueryInvestmentListByCompanyID ...
func (s *Server) QueryInvestmentListByCompanyID(ctx context.Context, request *cad.QueryInvestmentListByCompanyIDRequest) (*cad.InvestmentTransactionConnection, error) {
	var limit, offset, count int
	s.Repository.Client.Model(&models.Investments{}).Where("investor_permalink = ?", request.CompanyId).Count(&count)
	if count == 0 {
		return nil, errors.New("no datas")
	}
	if request.First != nil && request.Last != nil {
		return nil, errors.New("param first and last can't exist in same time!")
	}

	//把first和after的值转为limit和offset
	if request.First != nil {
		limit, offset = s.ChangeFirstAfterToLimitOffset(request.First, request.After)
	}

	//把last和before的值转为limit和offset
	if request.Last != nil {
		limit, offset = s.ChangeLastBeforeToLimitOffset(request.Last, request.Before, count)
	}

	inverstmentsDB, err := s.Repository.QueryInvestmentListByCompanyID(request.CompanyId, limit, offset)
	if err != nil {
		return nil, err
	}

	investmentConn := &cad.InvestmentTransactionConnection{}
	pageInfo := &cad.PageInfo{}

	if offset > 1 {
		pageInfo.HasPreviousPage = true
	} else {
		pageInfo.HasPreviousPage = false
	}

	if offset < count && offset+limit < count {
		pageInfo.HasNextPage = true
	} else {
		pageInfo.HasNextPage = false
	}
	pageInfo.StartCursor = &wrappers.StringValue{Value: strconv.Itoa(offset + 1)}
	pageInfo.EndCursor = &wrappers.StringValue{Value: strconv.Itoa(offset + limit)}

	investmentConn.PageInfo = pageInfo
	investmentConn.TotalCount = int64(len(inverstmentsDB))

	for i, investment := range inverstmentsDB {
		edge := &cad.InvestmentTransactionEdge{
			Node: &cad.InvestmentTransaction{
				InvertorInfo: &cad.CompanyInfo{
					CompanyName:  investment.CompanyName,
					CompanyId:    investment.CompanyPermalink,
					CategoryCode: investment.CompanyCategoryCode,
				},
				FinancingInfo: &cad.FinancingInfo{
					CompanyId:        investment.CompanyPermalink,
					FundingRoundType: investment.FundingRoundType,
					FundedAt:         investment.FundedAt,
					RaisedAmountUsd:  investment.RaisedAmountUsd,
				},
			},
			Cursor: strconv.Itoa(offset + i + 1),
		}
		investmentConn.Edges = append(investmentConn.Edges, edge)
	}
	return investmentConn, nil

}
