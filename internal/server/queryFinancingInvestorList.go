package server

import (
	context "context"
	"gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
	"log"
	"errors"
)

// QueryFinancingInvestorListByFinancingIndex 通过4个融资事件的组合键搜索该融资事件的所有投资者
func (s *Server) QueryFinancingInvestorListByFinancingIndex(ctx context.Context, request *cad.QueryFinancingInvestorListByFinancingIndexRequest) (*cad.InvestorListConnection, error) {
	totalCount, err := s.Repository.QueryFinancingInvestorsCountByFinancingIndex(
		request.CompanyId, 
		request.FundingRoundType, 
		request.FundedAt, 
		int(request.RaisedAmountUsd))
	if err != nil {
		return nil, err
	}

	res := &cad.InvestorListConnection {
		TotalCount: 0,
		Nodes: make([]*cad.Investor, 0),
		Edges: make([]*cad.InvestorEdge, 0),
		PageInfo: &cad.PageInfo{},
	}

	if *totalCount <= 0 {
		log.Println("没有投资者")
		return res, nil
	}
	res.TotalCount = int64(*totalCount)
	log.Println("投资者总数: " ,*totalCount)

	// 将first, after, last, before转换成limit，offset
	limit := 0
	offset := 0
	if request.First != nil && request.After != nil {
		if request.First.Value <= 0 || request.After.Value < 0 || request.After.Value >= int64(*totalCount) {
			log.Println("请设置合理的范围")
			return nil, errors.New("请设置合理的范围")
		}
		offset = int(request.After.Value)
		if request.First.Value + request.After.Value > int64(*totalCount) {
			limit = *totalCount - int(request.After.Value)
		} else {
			limit = int(request.First.Value)
		}
	} else if request.Last != nil && request.Before != nil {
		if request.Last.Value <= 0 || request.Before.Value < 0 || request.Before.Value >= int64(*totalCount) {
			log.Println("请设置合理的范围")
			return nil, errors.New("请设置合理的范围")
		}
		limit = int(request.Before.Value)
		if request.Before.Value - request.Last.Value < 0 {
			offset = 0
		} else {
			offset = int(request.Before.Value - request.Last.Value + 1)
		}
	}

	log.Println("Limit: ", limit)
	log.Println("Offset: ", offset)
	
	// 查询投资者
	investments, err := s.Repository.QueryFinancingInvestorsByFinancingIndex(
		request.CompanyId, 
		request.FundingRoundType, 
		request.FundedAt, 
		int(request.RaisedAmountUsd),
		limit,
		offset)
	if err != nil {
		return nil, err
	}
	
	// 数据封装
	for _, value := range investments {
		companyInfo := &cad.CompanyInfo {
			CompanyId: value.InvestorPermalink,
			CompanyName: value.InvestorName,
			CategoryCode: value.InvestorCategoryCode,
		}
		investor := &cad.Investor {
			CompanyInfo: companyInfo,
		}
		res.Nodes = append(res.Nodes, investor)
	}

	return res, nil
}