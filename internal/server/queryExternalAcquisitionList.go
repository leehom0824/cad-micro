package server

import (
	context "context"
	"gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

func (s *Server) QueryExternalAcquisitionListByCompanyID(ctx context.Context, request *cad.QueryExternalAcquisitionListByCompanyIDRequest) (*cad.ExternalAcquisitionConnection, error) {
	return nil, nil
}
