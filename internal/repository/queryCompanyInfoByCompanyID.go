package repository

import (
	models "gitlab.com/mayunmeiyouming/cad-micro/internal/models"
)

// QueryCompanyInfoByCompanyID 通过公司id查询公司所有信息
// 所有函数的接受者都应该是Database，所有函数除了返回有用的信息，还需要返回error方便调试
func (r *Database) QueryCompanyInfoByCompanyID(id string) (*models.Companies, error) {
	var res models.Companies
	// 数据库连接已经封装在Database的Client
	err := r.Client.Where("permalink = ?", id).Find(&res).Error
	if err != nil {
		return nil, err
	}
	return &res, nil
}
