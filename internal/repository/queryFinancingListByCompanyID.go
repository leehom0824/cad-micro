package repository

import (
	"gitlab.com/mayunmeiyouming/cad-micro/internal/models"
)

//QueryFinancingListByCompanyID ...
func (r *Database) QueryFinancingListByCompanyID(id string, limit int, offset int) ([]*models.CompaniesRounds, error) {
	var res []*models.CompaniesRounds
	r.Client.Table("companies").Select("companies.*,rounds.*").
		Joins("inner join rounds  on companies.permalink  = rounds.company_permalink").
		Where("rounds.company_permalink = ?", "/company/121nexus").
		Limit(limit).
		Offset(offset).
		Order("funded_at desc").
		Scan(&res)
	return res, nil
}

//QueryInvertorListByCompanyIDAndFundedAt .....
func (r *Database) QueryInvertorListByCompanyIDAndFundedAt(companyID string, fundedAt string) ([]*models.Investments, error) {
	var res []*models.Investments
	r.Client.Table("investments").Where("company_permalink = ?", companyID).
		Where("funded_at = ?", fundedAt).Scan(&res)
	return res, nil
}
