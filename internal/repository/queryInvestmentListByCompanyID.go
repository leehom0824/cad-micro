package repository

import (
	"gitlab.com/mayunmeiyouming/cad-micro/internal/models"
)

//QueryInvestmentListByCompanyID ...
func (r *Database) QueryInvestmentListByCompanyID(id string, limit int, offset int) ([]*models.Investments, error) {
	var res []*models.Investments
	r.Client.Table("investments").Where("investor_permalink = ?", id).Limit(limit).Offset(offset).Order("funded_at desc").Scan(&res)
	return res, nil
}
