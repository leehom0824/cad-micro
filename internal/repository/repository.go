package repository

import (
	"gitlab.com/mayunmeiyouming/cad-micro/internal/models"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// Database 是数据库连接的封装
type Database struct {
	Client gorm.DB
}

// QueryFundedInfosByCompanyIDAndFundedAt 通过融资方的id和融资日期查询融资数据
func (r *Database) QueryFundedInfosByCompanyIDAndFundedAt(id string, fundedAt string) ([]models.Rounds, error) {
	var res []models.Rounds
	// 数据库连接已经封装在Database的Client
	err := r.Client.Where("company_permalink = ? and funded_at = ?", id, fundedAt).Find(&res).Error
	if err != nil {
		return nil, err
	}
	return res, nil
}

// QueryFundedInfoByFinancingIndex 通过4个组合索引查询融资数据
func (r *Database) QueryFundedInfoByFinancingIndex(id string, fundingRoundType string, fundedAt string, raisedAmountUSD int) (*models.Rounds, error) {
	var res models.Rounds
	// 数据库连接已经封装在Database的Client
	err := r.Client.Where("company_permalink = ? and funding_round_type = ? and funded_at = ? and raised_amount_usd = ?", id, fundingRoundType, fundedAt, raisedAmountUSD).Find(&res).Error
	if err != nil {
		return nil, err
	}
	return &res, nil
}

// QueryCompanyListByCompanyName 通过公司名字进行模糊查找，并且返回符合的公司信息
func (r *Database) QueryCompanyListByCompanyName(name string) ([]models.Companies, error) {
	var res []models.Companies
	// 数据库连接已经封装在Database的Client
	err := r.Client.Where("name like ?", name + "%").Find(&res).Error
	if err != nil {
		return nil, err
	}
	return res, nil
}

// QueryFinancingInvestorsByFinancingIndex 通过4个融资事件的组合键搜索该融资事件的所有投资者
func (r *Database) QueryFinancingInvestorsByFinancingIndex(
	id string, 
	fundingRoundType string, 
	fundedAt string, 
	raisedAmountUSD int,
	limit int,
	offset int,
	) ([]models.Investments, error) {
	var res []models.Investments
	// 数据库连接已经封装在Database的Client
	err := r.Client.Where("company_permalink = ? and funding_round_type = ? and funded_at = ? and raised_amount_usd = ?", id, fundingRoundType, fundedAt, raisedAmountUSD).Limit(limit).Offset(offset).Find(&res).Error
	if err != nil {
		return nil, err
	}
	return res, nil
}

// QueryFinancingInvestorsCountByFinancingIndex 通过4个融资事件的组合键搜索该融资事件的所有投资者的数量
func (r *Database) QueryFinancingInvestorsCountByFinancingIndex(
	id string, 
	fundingRoundType string, 
	fundedAt string, 
	raisedAmountUSD int,
	) (*int, error) {

	var res int
	var investors []models.Investments

	err := r.Client.Where("company_permalink = ? and funding_round_type = ? and funded_at = ? and raised_amount_usd = ?", id, fundingRoundType, fundedAt, raisedAmountUSD).Find(&investors).Count(&res).Error
	if err != nil {
		return nil, err
	}
	return &res, nil
}