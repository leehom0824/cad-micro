package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/golang/protobuf/ptypes/wrappers"
	"github.com/prometheus/common/log"

	"gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

func main() {
	client := cad.NewCadTwirpProtobufClient("http://localhost:1234", &http.Client{})
	investmentListConn, err := client.QueryInvestmentListByCompanyID(
		context.Background(),
		&cad.QueryInvestmentListByCompanyIDRequest{
			CompanyId: "/company/10xelerator",
			Last:      &wrappers.Int64Value{Value: 2},
		},
	)
	if err != nil {
		log.Debug(err)
	}
	investmentListConnJSON, err := json.MarshalIndent(investmentListConn, "", "\t")
	if err != nil {
		log.Debug(err)
	}
	fmt.Println(string(investmentListConnJSON))

}
