package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	//"log"

	//"github.com/golang/protobuf/ptypes/empty"
	"github.com/prometheus/common/log"
	cad "gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

func main() {
	client := cad.NewCadTwirpProtobufClient("http://localhost:1234", &http.Client{})

	companyInfo, err := client.QueryCompanyOverviewByCompanyID(
		context.Background(),
		&cad.QueryCompanyOverviewByCompanyIDRequest {
			CompanyId: "/company/169-st",
		},
	)

	if err != nil {
		log.Debug(err)
	}

	companyInfoJson, err := json.MarshalIndent(companyInfo, "", "\t")
	if err != nil {
		log.Debug(err)
	}
	fmt.Println("CompanyOverview:")
	fmt.Println(string(companyInfoJson))
	fmt.Println("==================================================")
}