package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	//"log"

	//"github.com/golang/protobuf/ptypes/empty"
	"github.com/prometheus/common/log"
	cad "gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

func main() {
	client := cad.NewCadTwirpProtobufClient("http://localhost:1234", &http.Client{})

	financingOverview, err := client.QueryFinancingOverviewByFinancingIndex(
		context.Background(),
		&cad.QueryFinancingOverviewByFinancingIndexRequest {
			CompanyId: "/company/waywire",  
			FundingRoundType: "series-a",
			FundedAt: "2012-06-30",
			RaisedAmountUsd: 1750000,
		},
	)

	if err != nil {
		log.Debug(err)
	}

	financingOverviewJson, err := json.MarshalIndent(financingOverview, "", "\t")
	if err != nil {
		log.Debug(err)
	}
	fmt.Println("FinancingOverview:")
	fmt.Println(string(financingOverviewJson))
	fmt.Println("==================================================")
}